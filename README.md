# Vim snippets for UltiSnips

## Usage

1. Create `directory` of your chosen name (e.g. `snips`; do NOT use name `snippets`) in your main `.vim` directory and place your chosen `*.snippets` files in it.
2. Add `let g:UltiSnipsSnippetDirectories=['UltiSnips', 'directory']` to your `.vimrc`.
3. See *UltiSnips-how-snippets-are-loaded* section of [UltiSnips documentation](https://github.com/SirVer/ultisnips/blob/master/doc/UltiSnips.txt) for reference.

## This repository contains:

### php_wordpress.snippets

PHP snippets for WordPress:

- custom post type templates 
- plugin header

CSS snippets for WordPress:

- child theme header
